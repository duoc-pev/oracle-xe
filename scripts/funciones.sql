create or replace function fn_ajuste_digito_verificador(prefix in integer) return varchar2 is
    dv         varchar2(2);
    rut        integer := prefix;
    digito     integer := 0;
    contador   integer := 2;
    multiplo   integer := 0;
    acumulador integer := 0;

/******************************************************************************
   NAME:       fn_ajuste_digito_verificador
   PURPOSE:

   REVISIONS:
   Ver        Date        Author                        Description
   ---------  ----------  ---------------               --------------------------
	1.0       14/06/2019  a.cornejos@alumnos.duoc.cl    1. funcion para corregir el digito verificador de rut ya ingresados en tablas
******************************************************************************/

begin
    while rut != 0
        loop
            multiplo := mod(rut, 10) * contador;
            acumulador := acumulador + multiplo;
            rut := floor(rut / 10);
            contador := contador + 1;
            if (contador = 8) then
                contador := 2;
            end if;
        end loop;

    digito := 11 - mod(acumulador, 11);

    dv := to_char(digito);

    if digito = '10' then
        dv := 'K';
    elsif (digito = '11') then
        dv := '0';
    end if;

    return dv;

exception
    when others then
        return (-1);
end fn_ajuste_digito_verificador;


update CUSTOMERS set CUS_RUT_DV = fn_ajuste_digito_verificador(CUS_RUT);

update EMPLOYEE set EMP_RUT_DV = fn_ajuste_digito_verificador(EMP_RUT);

update SUPPLIERS set SUP_RUT_DV = fn_ajuste_digito_verificador(SUP_RUT);




create or replace function fn_ajuste_fecha_vencimiento(minDays in INTEGER, maxDays in INTEGER) return timestamp with time zone is

    actualDate timestamp with time zone := CURRENT_TIMESTAMP;
    finalDate timestamp with time zone;
    randomDays integer := round(DBMS_RANDOM.value(minDays, maxDays));

/******************************************************************************
   NAME:       fn_ajuste_fecha_vencimiento
   PURPOSE:

   REVISIONS:
   Ver        Date        Author                        Description
   ---------  ----------  ---------------               --------------------------
	1.0       14/06/2019  a.cornejos@alumnos.duoc.cl    1. funcion para corregir las fechas de vencimiento de los productos
******************************************************************************/

begin

    finalDate := actualDate + randomDays * interval '1' day;

    return finalDate;

end fn_ajuste_fecha_vencimiento;

update PRODUCTS set (PRODUCTS.product_expire_date) = fn_ajuste_fecha_vencimiento(30, 180);




CREATE OR REPLACE TRIGGER barcode_generator_trigger
    before INSERT or update
    ON BRANCH_INVENTORY
    FOR EACH ROW
DECLARE
    v_code   BRANCH_INVENTORY.bi_product_code%TYPE;
    v_sup_id integer;
BEGIN
    SELECT (
        lpad(sup_id, 3, 0) ||
        lpad(PROD_CAT_ID, 3, 0) ||
        nvl((extract(year from :new.BI_PRODUCT_EXPIRE) || lpad(extract(month from :new.BI_PRODUCT_EXPIRE), 2, 0) || lpad(extract(day from :new.BI_PRODUCT_EXPIRE), 2, 0)), 000000000) ||
        lpad(PROD_UN_ID, 3, 0))
    into v_code
    from PRODUCTS
    where PRODUCT_ID = :new.bi_product_id;

    update BRANCH_INVENTORY set bi_product_code = v_code where bi_id = :new.bi_id;
END barcode_generator_trigger;



create or replace procedure sp_insert_product_inventory(bi_branch_id in BRANCH_INVENTORY.BI_BRANCH_ID%type,
                                                        bi_product_id in BRANCH_INVENTORY.BI_PRODUCT_ID%type,
                                                        bi_quantity in BRANCH_INVENTORY.BI_QUANTITY%type,
                                                        bi_critical in BRANCH_INVENTORY.BI_QUANTITY%type,
                                                        bi_expire in BRANCH_INVENTORY.BI_PRODUCT_EXPIRE%type) as
    /*********************************************************************************************
   NAME: sp_insert_product_inventory
   PURPOSE:

   REVISIONS:
   Ver          Date           Author                       Description
   -----------  ----------    ---------------           -------------------------
    1.0       06/06/2019     a.cornejos@alumnos.duoc.cl       1. procedimiento que registra inventario de producto
***********************************************************************************************/

    v_product_code number;
BEGIN

    SELECT (
                   lpad(p.sup_id, 3, 0) ||
                   lpad(p.PROD_CAT_ID, 3, 0) ||
                   nvl((extract(year from bi_expire) || lpad(extract(month from bi_expire), 2, 0) ||
                        lpad(extract(day from bi_expire), 2, 0)), 000000000) ||
                   lpad(p.PROD_UN_ID, 3, 0)
        )
    into v_product_code
    from PRODUCTS p
    where p.product_id = bi_product_id;


    insert into BRANCH_INVENTORY(bi_id, bi_branch_id, bi_product_id, bi_quantity, bi_critical_q, bi_product_code,
                                 bi_product_expire)
    values (BRANCH_INVENTORY_ID_SEQ.nextval, bi_branch_id, bi_product_id, bi_quantity, bi_critical, v_product_code, bi_expire);

end sp_insert_product_inventory;


begin
    sp_insert_product_inventory(1, 1, 100, 10, current_timestamp);

end ;


