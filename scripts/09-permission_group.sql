insert into perm_group (PGROUP_ID, pgroup_name, pgroup_desc) values (1, 'Dashboards', 'Grupo de permisos para el componente dashboard del backoffice');
insert into perm_group (PGROUP_ID, pgroup_name, pgroup_desc) values (2, 'Inventario', 'Grupo de permisos para el componente inventario del backoffice');
insert into perm_group (PGROUP_ID, pgroup_name, pgroup_desc) values (3, 'Proveedores', 'Grupo de permisos para el componente proveedores del backoffice');
insert into perm_group (PGROUP_ID, pgroup_name, pgroup_desc) values (4, 'Ordenes de Compra', 'Grupo de permisos para el componente ordenes de compra del backoffice');
insert into perm_group (PGROUP_ID, pgroup_name, pgroup_desc) values (5, 'Transacciones', 'Grupo de permisos para el componente de transacciones del backoffice');
insert into perm_group (PGROUP_ID, pgroup_name, pgroup_desc) values (6, 'Usuarios', 'Grupo de permisos para el componente de usuarios del backoffice');
insert into perm_group (PGROUP_ID, pgroup_name, pgroup_desc) values (7, 'Clientes', 'Grupo de permisos para el modulo de clientes en el backoffice');
insert into perm_group (PGROUP_ID, pgroup_name, pgroup_desc) values (8, 'Reportes', 'Grupo de permisos para el componente de reportes del backoffice');
insert into perm_group (PGROUP_ID, pgroup_name, pgroup_desc) values (9, 'Configuración', 'Grupo de permisos para la configuración del backoffice');
