insert into ORDER_STATUS (os_id, os_name, os_description, os_order) values (1, 'Pendiente', null, 1);
insert into ORDER_STATUS (os_id, os_name, os_description, os_order) values (2, 'Esperando Pago', null, 2);
insert into ORDER_STATUS (os_id, os_name, os_description, os_order) values (3, 'Completada', null, 3);
insert into ORDER_STATUS (os_id, os_name, os_description, os_order) values (4, 'Cancelada', null, 4);
insert into ORDER_STATUS (os_id, os_name, os_description, os_order) values (5, 'Declinada', null, 5);
insert into ORDER_STATUS (os_id, os_name, os_description, os_order) values (6, 'Anulada', null, 6);
