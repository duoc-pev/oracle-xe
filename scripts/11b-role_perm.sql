-- administrador dashboard
insert into role_perm(role_id, perm_id) values (1, 1);
insert into role_perm(role_id, perm_id) values (1, 2);
insert into role_perm(role_id, perm_id) values (1, 3);
insert into role_perm(role_id, perm_id) values (1, 4);
-- administrador inventario
insert into role_perm(role_id, perm_id) values (1, 5);
insert into role_perm(role_id, perm_id) values (1, 6);
insert into role_perm(role_id, perm_id) values (1, 7);
insert into role_perm(role_id, perm_id) values (1, 8);
-- administrador proveedores
insert into role_perm(role_id, perm_id) values (1, 9);
insert into role_perm(role_id, perm_id) values (1, 10);
insert into role_perm(role_id, perm_id) values (1, 11);
insert into role_perm(role_id, perm_id) values (1, 12);
-- administrador oc
insert into role_perm(role_id, perm_id) values (1, 13);
insert into role_perm(role_id, perm_id) values (1, 14);
insert into role_perm(role_id, perm_id) values (1, 15);
insert into role_perm(role_id, perm_id) values (1, 16);
-- administrador transacciones
insert into role_perm(role_id, perm_id) values (1, 17);
insert into role_perm(role_id, perm_id) values (1, 18);
insert into role_perm(role_id, perm_id) values (1, 19);
insert into role_perm(role_id, perm_id) values (1, 20);
-- administrador usuarios
insert into role_perm(role_id, perm_id) values (1, 21);
insert into role_perm(role_id, perm_id) values (1, 22);
insert into role_perm(role_id, perm_id) values (1, 23);
insert into role_perm(role_id, perm_id) values (1, 24);
-- administrador clientes
insert into role_perm(role_id, perm_id) values (1, 25);
insert into role_perm(role_id, perm_id) values (1, 26);
insert into role_perm(role_id, perm_id) values (1, 27);
insert into role_perm(role_id, perm_id) values (1, 28);
-- administrador reportes
insert into role_perm(role_id, perm_id) values (1, 29);
insert into role_perm(role_id, perm_id) values (1, 30);
insert into role_perm(role_id, perm_id) values (1, 31);
insert into role_perm(role_id, perm_id) values (1, 32);
-- administrador configuraciones
insert into role_perm(role_id, perm_id) values (1, 33);
insert into role_perm(role_id, perm_id) values (1, 34);
insert into role_perm(role_id, perm_id) values (1, 35);
insert into role_perm(role_id, perm_id) values (1, 36);

-- Gerente dashboard
insert into role_perm(role_id, perm_id) values (2, 1);
insert into role_perm(role_id, perm_id) values (2, 2);
insert into role_perm(role_id, perm_id) values (2, 3);
insert into role_perm(role_id, perm_id) values (2, 4);
-- Gerente inventario
insert into role_perm(role_id, perm_id) values (2, 5);
insert into role_perm(role_id, perm_id) values (2, 6);
insert into role_perm(role_id, perm_id) values (2, 7);
-- Gerente proveedores
insert into role_perm(role_id, perm_id) values (2, 9);
insert into role_perm(role_id, perm_id) values (2, 10);
insert into role_perm(role_id, perm_id) values (2, 11);
-- Gerente oc
insert into role_perm(role_id, perm_id) values (2, 13);
insert into role_perm(role_id, perm_id) values (2, 14);
insert into role_perm(role_id, perm_id) values (2, 15);
-- Gerente transacciones
insert into role_perm(role_id, perm_id) values (2, 18);
-- Gerente usuarios
insert into role_perm(role_id, perm_id) values (2, 21);
insert into role_perm(role_id, perm_id) values (2, 22);
insert into role_perm(role_id, perm_id) values (2, 23);
-- Gerente clientes
insert into role_perm(role_id, perm_id) values (2, 25);
insert into role_perm(role_id, perm_id) values (2, 26);
insert into role_perm(role_id, perm_id) values (2, 27);
-- Gerente reportes
insert into role_perm(role_id, perm_id) values (2, 29);
insert into role_perm(role_id, perm_id) values (2, 30);
-- Gerente configuraciones
insert into role_perm(role_id, perm_id) values (2, 34);
insert into role_perm(role_id, perm_id) values (2, 35);

-- Supervisor dashboard
insert into role_perm(role_id, perm_id) values (3, 2);
-- Supervisor inventario
insert into role_perm(role_id, perm_id) values (3, 5);
insert into role_perm(role_id, perm_id) values (3, 6);
insert into role_perm(role_id, perm_id) values (3, 7);
-- Supervisor proveedores
insert into role_perm(role_id, perm_id) values (3, 9);
insert into role_perm(role_id, perm_id) values (3, 10);
insert into role_perm(role_id, perm_id) values (3, 11);
-- Supervisor oc
insert into role_perm(role_id, perm_id) values (3, 13);
insert into role_perm(role_id, perm_id) values (3, 14);
insert into role_perm(role_id, perm_id) values (3, 15);
-- Supervisor transacciones
insert into role_perm(role_id, perm_id) values (3, 18);
insert into role_perm(role_id, perm_id) values (3, 19);
-- Supervisor usuarios
insert into role_perm(role_id, perm_id) values (3, 21);
insert into role_perm(role_id, perm_id) values (3, 22);
insert into role_perm(role_id, perm_id) values (3, 23);
-- Supervisor clientes
insert into role_perm(role_id, perm_id) values (3, 25);
insert into role_perm(role_id, perm_id) values (3, 26);
insert into role_perm(role_id, perm_id) values (3, 27);
-- Supervisor reportes
insert into role_perm(role_id, perm_id) values (3, 29);
insert into role_perm(role_id, perm_id) values (3, 30);
insert into role_perm(role_id, perm_id) values (3, 31);
-- Supervisor configuraciones
insert into role_perm(role_id, perm_id) values (3, 34);

-- Empleado dashboard
insert into role_perm(role_id, perm_id) values (4, 2);
-- Empleado inventario
insert into role_perm(role_id, perm_id) values (4, 5);
insert into role_perm(role_id, perm_id) values (4, 6);
insert into role_perm(role_id, perm_id) values (4, 7);
-- Empleado proveedores
insert into role_perm(role_id, perm_id) values (4, 10);
insert into role_perm(role_id, perm_id) values (4, 11);
-- Empleado oc
insert into role_perm(role_id, perm_id) values (4, 14);
insert into role_perm(role_id, perm_id) values (4, 15);
-- Empleado transacciones
insert into role_perm(role_id, perm_id) values (4, 17);
insert into role_perm(role_id, perm_id) values (4, 18);
insert into role_perm(role_id, perm_id) values (4, 19);
-- Empleado usuarios
-- Empleado clientes
insert into role_perm(role_id, perm_id) values (4, 25);
insert into role_perm(role_id, perm_id) values (4, 26);
insert into role_perm(role_id, perm_id) values (4, 27);
-- Empleado reportes
-- Empleado configuraciones

-- Vendedor dashboard
insert into role_perm(role_id, perm_id) values (5, 2);
-- Vendedor inventario
insert into role_perm(role_id, perm_id) values (5, 5);
insert into role_perm(role_id, perm_id) values (5, 6);
insert into role_perm(role_id, perm_id) values (5, 7);
-- Vendedor proveedores
insert into role_perm(role_id, perm_id) values (5, 10);
-- Vendedor oc
insert into role_perm(role_id, perm_id) values (5, 14);
insert into role_perm(role_id, perm_id) values (5, 15);
-- Vendedor transacciones
insert into role_perm(role_id, perm_id) values (5, 17);
insert into role_perm(role_id, perm_id) values (5, 18);
insert into role_perm(role_id, perm_id) values (5, 19);
-- Vendedor usuarios
-- Vendedor clientes
insert into role_perm(role_id, perm_id) values (5, 25);
insert into role_perm(role_id, perm_id) values (5, 26);
insert into role_perm(role_id, perm_id) values (5, 27);
-- Vendedor reportes
-- Vendedor configuraciones
