CREATE OR REPLACE TRIGGER gender_id_seq_trigger
  BEFORE INSERT
  ON GENDER
  FOR EACH ROW
  WHEN (new.gender_id is null)
DECLARE
  v_id GENDER.gender_id%TYPE;
BEGIN
  SELECT GENDER_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.gender_id := v_id;
END gender_id_seq_trigger;

CREATE OR REPLACE TRIGGER perm_group_id_seq_trigger
  BEFORE INSERT
  ON PERM_GROUP
  FOR EACH ROW
  WHEN (new.PGROUP_ID is null)
DECLARE
  v_id PERM_GROUP.PGROUP_ID%TYPE;
BEGIN
  SELECT PERM_GROUP_PGROUP_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.pgroup_id := v_id;
END perm_group_id_seq_trigger;

CREATE OR REPLACE TRIGGER permission_id_seq_trigger
  BEFORE INSERT
  ON PERMISSION
  FOR EACH ROW
  WHEN (new.PERM_ID is null)
DECLARE
  v_id PERMISSION.PERM_ID%TYPE;
BEGIN
  SELECT permission_perm_id_seq.nextval INTO v_id FROM DUAL;
  :new.PERM_ID := v_id;
END permission_id_seq_trigger;

CREATE OR REPLACE TRIGGER config_id_seq_trigger
  BEFORE INSERT
  ON configuration
  FOR EACH ROW
  WHEN (new.config_id is null)
DECLARE
  v_id configuration.config_id%TYPE;
BEGIN
  SELECT CONFIGURATION_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.config_id := v_id;
END config_id_seq_trigger;

CREATE OR REPLACE TRIGGER prod_unit_id_seq_trigger
  BEFORE INSERT
  ON prod_unit
  FOR EACH ROW
  WHEN (new.prod_un_id is null)
DECLARE
  v_id prod_unit.prod_un_id%TYPE;
BEGIN
  SELECT PROD_UNIT_PROD_UN_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.prod_un_id := v_id;
END prod_unit_id_seq_trigger;

CREATE OR REPLACE TRIGGER phone_id_seq_trigger
  BEFORE INSERT
  ON phone
  FOR EACH ROW
  WHEN (new.phone_id is null)
DECLARE
  v_id phone.phone_id%TYPE;
BEGIN
  SELECT PHONE_PHONE_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.phone_id := v_id;
END phone_id_seq_trigger;

CREATE OR REPLACE TRIGGER sos_id_seq_trigger
  BEFORE INSERT
  ON supplier_order_status
  FOR EACH ROW
  WHEN (new.so_status_id is null)
DECLARE
  v_id supplier_order_status.so_status_id%TYPE;
BEGIN
  SELECT SOS_SO_STATUS_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.so_status_id := v_id;
END sos_id_seq_trigger;

CREATE OR REPLACE TRIGGER roles_id_seq_trigger
  BEFORE INSERT
  ON roles
  FOR EACH ROW
  WHEN (new.role_id is null)
DECLARE
  v_id roles.role_id%TYPE;
BEGIN
  SELECT roles_role_id_seq.nextval INTO v_id FROM DUAL;
  :new.role_id := v_id;
END roles_id_seq_trigger;

CREATE OR REPLACE TRIGGER cus_pay_id_seq_trigger
  BEFORE INSERT
  ON cust_payments
  FOR EACH ROW
  WHEN (new.payment_id is null)
DECLARE
  v_id cust_payments.payment_id%TYPE;
BEGIN
  SELECT CUSTOMER_PAYMENTS_PAY_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.payment_id := v_id;
END cus_pay_id_seq_trigger;

CREATE OR REPLACE TRIGGER brands_id_seq_trigger
  BEFORE INSERT
  ON brands
  FOR EACH ROW
  WHEN (new.brand_id is null)
DECLARE
  v_id brands.brand_id%TYPE;
BEGIN
  SELECT BRANDS_BRAND_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.brand_id := v_id;
END brands_id_seq_trigger;

CREATE OR REPLACE TRIGGER brands_id_seq_trigger
  BEFORE INSERT
  ON brands
  FOR EACH ROW
  WHEN (new.brand_id is null)
DECLARE
  v_id brands.brand_id%TYPE;
BEGIN
  SELECT BRANDS_BRAND_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.brand_id := v_id;
END brands_id_seq_trigger;

CREATE OR REPLACE TRIGGER position_id_seq_trigger
  BEFORE INSERT
  ON position
  FOR EACH ROW
  WHEN (new.pos_id is null)
DECLARE
  v_id position.pos_id%TYPE;
BEGIN
  SELECT POSITION_POS_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.pos_id := v_id;
END position_id_seq_trigger;

CREATE OR REPLACE TRIGGER prod_cat_id_seq_trigger
  BEFORE INSERT
  ON prod_category
  FOR EACH ROW
  WHEN (new.prod_cat_id is null)
DECLARE
  v_id prod_category.prod_cat_id%TYPE;
BEGIN
  SELECT PROD_CATEGORY_PROD_CAT_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.prod_cat_id := v_id;
END prod_cat_id_seq_trigger;

CREATE OR REPLACE TRIGGER o_status_id_seq_trigger
  BEFORE INSERT
  ON order_status
  FOR EACH ROW
  WHEN (new.os_id is null)
DECLARE
  v_id order_status.os_id%TYPE;
BEGIN
  SELECT ORDER_STATUS_OS_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.os_id := v_id;
END o_status_id_seq_trigger;

CREATE OR REPLACE TRIGGER district_id_seq_trigger
  BEFORE INSERT
  ON district
  FOR EACH ROW
  WHEN (new.district_id is null)
DECLARE
  v_id district.district_id%TYPE;
BEGIN
  SELECT district_id_seq.nextval INTO v_id FROM DUAL;
  :new.district_id := v_id;
END district_id_seq_trigger;

CREATE OR REPLACE TRIGGER city_id_seq_trigger
  BEFORE INSERT
  ON city
  FOR EACH ROW
  WHEN (new.city_id is null)
DECLARE
  v_id city.city_id%TYPE;
BEGIN
  SELECT city_city_id_seq.nextval INTO v_id FROM DUAL;
  :new.city_id := v_id;
END city_id_seq_trigger;

CREATE OR REPLACE TRIGGER commune_id_seq_trigger
  BEFORE INSERT
  ON commune
  FOR EACH ROW
  WHEN (new.com_id is null)
DECLARE
  v_id commune.com_id%TYPE;
BEGIN
  SELECT commune_com_id_seq.nextval INTO v_id FROM DUAL;
  :new.com_id := v_id;
END commune_id_seq_trigger;

CREATE OR REPLACE TRIGGER address_id_seq_trigger
  BEFORE INSERT
  ON addresses
  FOR EACH ROW
  WHEN (new.add_id is null)
DECLARE
  v_id addresses.add_id%TYPE;
BEGIN
  SELECT ADDRESSES_ADD_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.add_id := v_id;
END address_id_seq_trigger;

CREATE OR REPLACE TRIGGER suppliers_id_seq_trigger
    BEFORE INSERT
    ON suppliers
    FOR EACH ROW
    WHEN (new.sup_id is null)
DECLARE
    v_id suppliers.sup_id%TYPE;
BEGIN
    SELECT SUPPLIERS_ID_SEQ.nextval INTO v_id FROM DUAL;
    :new.sup_id := v_id;
END suppliers_id_seq_trigger;

CREATE OR REPLACE TRIGGER sup_emp_id_seq_trigger
    BEFORE INSERT
    ON sup_employees
    FOR EACH ROW
    WHEN (new.se_emp_id is null)
DECLARE
    v_id sup_employees.se_emp_id%TYPE;
BEGIN
    SELECT SUP_EMPLOYEES_SE_EMP_ID_SEQ.nextval INTO v_id FROM DUAL;
    :new.se_emp_id := v_id;
END sup_emp_id_seq_trigger;

CREATE OR REPLACE TRIGGER branch_off_id_seq_trigger
  BEFORE INSERT
  ON branch_office
  FOR EACH ROW
  WHEN (new.branch_id is null)
DECLARE
  v_id branch_office.branch_id%TYPE;
BEGIN
  SELECT branch_office_branch_id_seq.nextval INTO v_id FROM DUAL;
  :new.branch_id := v_id;
END branch_off_id_seq_trigger;

CREATE OR REPLACE TRIGGER sup_orders_id_seq_trigger
  BEFORE INSERT
  ON supplier_orders
  FOR EACH ROW
  WHEN (new.so_id is null)
DECLARE
  v_id supplier_orders.so_id%TYPE;
BEGIN
  SELECT SUPPLIER_ORDERS_SO_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.so_id := v_id;
END sup_orders_id_seq_trigger;

CREATE OR REPLACE TRIGGER products_id_seq_trigger
  BEFORE INSERT
  ON products
  FOR EACH ROW
  WHEN (new.product_id is null)
DECLARE
  v_id products.product_id%TYPE;
BEGIN
  SELECT PRODUCTS_PRODUCT_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.product_id := v_id;
END products_id_seq_trigger;

CREATE OR REPLACE TRIGGER prod_price_id_seq_trigger
  BEFORE INSERT
  ON product_price
  FOR EACH ROW
  WHEN (new.pp_id is null)
DECLARE
  v_id product_price.pp_id%TYPE;
BEGIN
  SELECT PRODUCT_PRICE_PP_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.pp_id := v_id;
END prod_price_id_seq_trigger;

CREATE OR REPLACE TRIGGER sod_id_seq_trigger
  BEFORE INSERT
  ON supplier_order_detail
  FOR EACH ROW
  WHEN (new.sod_id is null)
DECLARE
  v_id supplier_order_detail.sod_id%TYPE;
BEGIN
  SELECT SOD_PRODUCT_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.sod_id := v_id;
END sod_id_seq_trigger;

CREATE OR REPLACE TRIGGER employee_id_seq_trigger
  BEFORE INSERT
  ON employee
  FOR EACH ROW
  WHEN (new.emp_id is null)
DECLARE
  v_id employee.emp_id%TYPE;
BEGIN
  SELECT EMPLOYEE_EMP_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.emp_id := v_id;
END employee_id_seq_trigger;

CREATE OR REPLACE TRIGGER prod_aud_id_seq_trigger
  BEFORE INSERT
  ON product_audit
  FOR EACH ROW
  WHEN (new.prod_au_id is null)
DECLARE
  v_id product_audit.prod_au_id%TYPE;
BEGIN
  SELECT product_audit_prod_au_id_seq.nextval INTO v_id FROM DUAL;
  :new.prod_au_id := v_id;
END prod_aud_id_seq_trigger;

CREATE OR REPLACE TRIGGER emp_aud_id_seq_trigger
  BEFORE INSERT
  ON employee_audit
  FOR EACH ROW
  WHEN (new.eaudit_id is null)
DECLARE
  v_id employee_audit.eaudit_id%TYPE;
BEGIN
  SELECT EMPLOYEE_AUDIT_EAUDIT_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.eaudit_id := v_id;
END emp_aud_id_seq_trigger;

CREATE OR REPLACE TRIGGER sup_aud_id_seq_trigger
  BEFORE INSERT
  ON supplier_audit
  FOR EACH ROW
  WHEN (new.sa_id is null)
DECLARE
  v_id supplier_audit.sa_id%TYPE;
BEGIN
  SELECT so_sup_order_id_seq.nextval INTO v_id FROM DUAL;
  :new.sa_id := v_id;
END sup_aud_id_seq_trigger;

CREATE OR REPLACE TRIGGER cus_id_seq_trigger
  BEFORE INSERT
  ON CUSTOMERS
  FOR EACH ROW
  WHEN (new.cus_id is null)
DECLARE
  v_id customers.cus_id%TYPE;
BEGIN
  SELECT CUSTOMERS_USER_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.cus_id := v_id;
END cus_id_seq_trigger;

CREATE OR REPLACE TRIGGER cus_aud_id_seq_trigger
  BEFORE INSERT
  ON customer_audit
  FOR EACH ROW
  WHEN (new.caud_id is null)
DECLARE
  v_id customer_audit.caud_id%TYPE;
BEGIN
  SELECT CUSTOMER_AUDIT_CAUD_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.caud_id := v_id;
END cus_aud_id_seq_trigger;

CREATE OR REPLACE TRIGGER cus_order_id_seq_trigger
  BEFORE INSERT
  ON cust_orders
  FOR EACH ROW
  WHEN (new.order_id is null)
DECLARE
  v_id cust_orders.order_id%TYPE;
BEGIN
  SELECT CUST_ORDERS_ORDER_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.order_id := v_id;
END cus_order_id_seq_trigger;

CREATE OR REPLACE TRIGGER tickets_id_seq_trigger
  BEFORE INSERT
  ON tickets
  FOR EACH ROW
  WHEN (new.ticket_id is null)
DECLARE
  v_id tickets.ticket_id%TYPE;
BEGIN
  SELECT tickets_TICKET_ID_SEQ.nextval INTO v_id FROM DUAL;
  :new.ticket_id := v_id;
END tickets_id_seq_trigger;

CREATE OR REPLACE TRIGGER ord_aud_id_seq_trigger
  BEFORE INSERT
  ON order_audit
  FOR EACH ROW
  WHEN (new.orau_order_id is null)
DECLARE
  v_id order_audit.orau_order_id%TYPE;
BEGIN
  SELECT order_audit_id_seq.nextval INTO v_id FROM DUAL;
  :new.orau_order_id := v_id;
END ord_aud_id_seq_trigger;

CREATE OR REPLACE TRIGGER cus_ord_det_id_seq_trigger
  BEFORE INSERT
  ON cust_order_det
  FOR EACH ROW
  WHEN (new.cod_product_id is null)
DECLARE
  v_id cust_order_det.cod_product_id%TYPE;
BEGIN
  SELECT cust_order_det_id_seq.nextval INTO v_id FROM DUAL;
  :new.cod_product_id := v_id;
END cus_ord_det_id_seq_trigger;

CREATE OR REPLACE TRIGGER branch_inv_id_seq_trigger
  BEFORE INSERT
  ON branch_inventory
  FOR EACH ROW
  WHEN (new.bi_id is null)
DECLARE
  v_id branch_inventory.bi_id%TYPE;
BEGIN
  SELECT branch_inventory_id_seq.nextval INTO v_id FROM DUAL;
  :new.bi_id := v_id;
END branch_inv_id_seq_trigger;


