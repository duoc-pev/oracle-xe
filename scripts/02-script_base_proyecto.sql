
CREATE SEQUENCE GENDER_ID_SEQ;

CREATE TABLE gender (
                gender_id NUMBER NOT NULL,
                gender_name VARCHAR2(20) NOT NULL,
                CONSTRAINT GENDER_PK PRIMARY KEY (gender_id)
);
COMMENT ON TABLE gender IS 'Tabla con los tipos de genero disponible';


CREATE SEQUENCE PERM_GROUP_PGROUP_ID_SEQ;

CREATE TABLE perm_group (
                pgroup_id NUMBER NOT NULL,
                pgroup_name VARCHAR2(40) NOT NULL,
                pgroup_desc VARCHAR2(255) NOT NULL,
                CONSTRAINT PERM_GROUP_PK PRIMARY KEY (pgroup_id)
);
COMMENT ON TABLE perm_group IS 'Tabla que agrupa los permisos según concepto de negocio';


CREATE SEQUENCE PERMISSION_PERM_ID_SEQ;

CREATE TABLE permission (
                perm_id NUMBER NOT NULL,
                perm_pgroup_id NUMBER NOT NULL,
                perm_active CHAR(1) DEFAULT 'Y' NOT NULL,
                perm_name VARCHAR2(30) NOT NULL,
                perm_description VARCHAR2(255),
                CONSTRAINT PERMISSION_PK PRIMARY KEY (perm_id)
);
COMMENT ON TABLE permission IS 'Tabla con los permisos para ser asignados en los roles';


CREATE SEQUENCE CONFIGURATION_ID_SEQ;

CREATE TABLE configuration (
                config_id NUMBER NOT NULL,
                config_logo BLOB,
                config_store_name VARCHAR2(60) DEFAULT 'Mi Almacen' NOT NULL,
                config_alert_limit NUMBER DEFAULT 10 NOT NULL,
                CONSTRAINT CONFIG_PK PRIMARY KEY (config_id)
);
COMMENT ON TABLE configuration IS 'Tabla de configuración para personalización de funcionalidades del usuario';


CREATE SEQUENCE PROD_UNIT_PROD_UN_ID_SEQ;

CREATE TABLE prod_unit (
                prod_un_id NUMBER NOT NULL,
                prod_un_name VARCHAR2(20) NOT NULL,
                prod_un_desc VARCHAR2(255),
                CONSTRAINT PROD_UNIT_PK PRIMARY KEY (prod_un_id)
);
COMMENT ON TABLE prod_unit IS 'Unidad de medida para el producto';


CREATE SEQUENCE PHONE_PHONE_ID_SEQ;

CREATE TABLE phone (
                phone_id NUMBER NOT NULL,
                phone_country_code NUMBER DEFAULT 56 NOT NULL,
                phone_area_code NUMBER DEFAULT 9 NOT NULL,
                phone_number NUMBER NOT NULL,
                phone_whatsapp CHAR(1) DEFAULT 'Y' NOT NULL,
                CONSTRAINT PHONE_PK PRIMARY KEY (phone_id)
);
COMMENT ON TABLE phone IS 'Tabla para guardar los numeros telefonicos';


CREATE SEQUENCE SOS_SO_STATUS_ID_SEQ;

CREATE TABLE supplier_order_status (
                so_status_id NUMBER NOT NULL,
                so_status_name VARCHAR2(60) NOT NULL,
                so_status_description VARCHAR2(255),
                so_status_level NUMBER NOT NULL,
                CONSTRAINT SUPP_ORD_STA_PK PRIMARY KEY (so_status_id)
);
COMMENT ON TABLE supplier_order_status IS 'Tabla con la informacion de los estatus disponibles para las ordenes de compra de los proveedores';


CREATE SEQUENCE ROLES_ROLE_ID_SEQ;

CREATE TABLE roles (
                role_id NUMBER NOT NULL,
                role_name VARCHAR2(30) NOT NULL,
                role_description VARCHAR2(255),
                CONSTRAINT ROLES_PK PRIMARY KEY (role_id)
);
COMMENT ON TABLE roles IS 'Tabla diccionario con los roles disponibles para los empleados de los locales';


CREATE TABLE role_perm (
                role_id NUMBER NOT NULL,
                perm_id NUMBER NOT NULL,
                CONSTRAINT RP_PK PRIMARY KEY (role_id, perm_id)
);
COMMENT ON TABLE role_perm IS 'Tabla intermedia para validar roles y permisos';


CREATE SEQUENCE CUSTOMER_PAYMENTS_PAY_ID_SEQ;

CREATE TABLE cust_payments (
                payment_id NUMBER NOT NULL,
                payment_name VARCHAR2(40) NOT NULL,
                payment_desc VARCHAR2(255),
                CONSTRAINT CUST_PAY_PK PRIMARY KEY (payment_id)
);
COMMENT ON TABLE cust_payments IS 'Tabla con la informacion de las formas de pago disponibles para los clientes';


CREATE SEQUENCE BRANDS_BRAND_ID_SEQ;

CREATE TABLE brands (
                brand_id NUMBER NOT NULL,
                brand_sname VARCHAR2(30) NOT NULL,
                brand_fname VARCHAR2(255) NOT NULL,
                CONSTRAINT BRANDS_PK PRIMARY KEY (brand_id)
);
COMMENT ON TABLE brands IS 'Tabla diccionario con las marcas disponibles para los productos';


CREATE SEQUENCE POSITION_POS_ID_SEQ;

CREATE TABLE position (
                pos_id NUMBER NOT NULL,
                pos_name VARCHAR2(60) NOT NULL,
                pos_description VARCHAR2(255),
                CONSTRAINT POSITION_PK PRIMARY KEY (pos_id)
);
COMMENT ON TABLE position IS 'Tabla diccionario con la información de los cargos disponibles para los contactos de los proveedores';


CREATE SEQUENCE PROD_CATEGORY_PROD_CAT_ID_SEQ;

CREATE TABLE prod_category (
                prod_cat_id NUMBER NOT NULL,
                prod_cat_name VARCHAR2(100) NOT NULL,
                prod_cat_desc VARCHAR2(255),
                prod_cat_parent_id NUMBER DEFAULT null,
                CONSTRAINT PROD_CAT_PK PRIMARY KEY (prod_cat_id)
);
COMMENT ON TABLE prod_category IS 'Tabla con las categorias de productos disponibles para los almacenes';


CREATE SEQUENCE ORDER_STATUS_OS_ID_SEQ;

CREATE TABLE order_status (
                os_id NUMBER NOT NULL,
                os_name VARCHAR2(30) NOT NULL,
                os_description VARCHAR2(255),
                os_order NUMBER NOT NULL,
                CONSTRAINT ORD_STA_PK PRIMARY KEY (os_id)
);
COMMENT ON TABLE order_status IS 'Tabla con los estatus disponibles para las compras';


CREATE SEQUENCE DISTRICT_ID_SEQ;

CREATE TABLE district (
                district_id NUMBER NOT NULL,
                district_order NUMBER NOT NULL,
                district_roman VARCHAR2(6) NOT NULL,
                district_name VARCHAR2(50) NOT NULL,
                CONSTRAINT DIS_PK PRIMARY KEY (district_id)
);
COMMENT ON TABLE district IS 'Tabla con la informacion de Regiones';

create sequence city_city_id_seq;

CREATE TABLE city (
                city_id NUMBER NOT NULL,
                city_district_id NUMBER NOT NULL,
                city_name VARCHAR2(50) NOT NULL,
                CONSTRAINT CITY_PK PRIMARY KEY (city_id)
);
COMMENT ON TABLE city IS 'Tabla con la informacion de Cuidades';

create sequence commune_com_id_seq;

CREATE TABLE commune (
                com_id NUMBER NOT NULL,
                com_city_id NUMBER NOT NULL,
                com_name VARCHAR2(50) NOT NULL,
                CONSTRAINT COMM_PK PRIMARY KEY (com_id)
);
COMMENT ON TABLE commune IS 'Tabla con la informacion de Comunas';


CREATE SEQUENCE ADDRESSES_ADD_ID_SEQ;

CREATE TABLE addresses (
                add_id NUMBER NOT NULL,
                add_com_id NUMBER NOT NULL,
                add_line_1 VARCHAR2(100) NOT NULL,
                add_line_2 VARCHAR2(100),
                add_postal_code NUMBER,
                add_created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                add_updated_at TIMESTAMP WITH TIME ZONE,
                CONSTRAINT ADD_PK PRIMARY KEY (add_id)
);
COMMENT ON TABLE addresses IS 'Tabla con la informacion de direcciones';


CREATE SEQUENCE SUPPLIERS_ID_SEQ;

CREATE TABLE suppliers (
                sup_id NUMBER NOT NULL,
                sup_name VARCHAR2(50) NOT NULL,
                sup_rut VARCHAR2(8) NOT NULL,
                sup_rut_dv VARCHAR2(1) NOT NULL,
                sup_other_details VARCHAR2(255),
                sup_add_id NUMBER NOT NULL,
                sup_phone_id NUMBER NOT NULL,
                CONSTRAINT SUPP_PK PRIMARY KEY (sup_id)
);
COMMENT ON TABLE suppliers IS 'Tabla con los proveedores disponibles para los almacenes';


CREATE UNIQUE INDEX suppliers_rut_idx
 ON suppliers
 ( sup_rut ASC );

CREATE SEQUENCE PRODUCTS_PRODUCT_ID_SEQ;

CREATE TABLE products (
                product_id NUMBER NOT NULL,
                product_sku VARCHAR2(30) NOT NULL,
                product_code NUMBER NOT NULL,
                product_name VARCHAR2(50) NOT NULL,
                product_description VARCHAR2(255),
                product_quantity NUMBER NOT NULL,
                product_expire_date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                brand_id NUMBER NOT NULL,
                sup_id NUMBER NOT NULL,
                prod_cat_id NUMBER NOT NULL,
                prod_un_id NUMBER NOT NULL,
                prod_active CHAR(1) DEFAULT 'Y' NOT NULL,
                CONSTRAINT PROD_PK PRIMARY KEY (product_id)
);
COMMENT ON TABLE products IS 'Tabla con la informacion de los productos disponibles';


CREATE UNIQUE INDEX products_code_idx
 ON products
 ( product_code ASC );

CREATE SEQUENCE PRODUCT_PRICE_PP_ID_SEQ;

CREATE TABLE product_price (
                pp_id NUMBER NOT NULL,
                pp_product_id NUMBER NOT NULL,
                pp_full_price NUMBER NOT NULL,
                pp_net_price NUMBER(6,2) NOT NULL,
                pp_tax_price NUMBER(6,2) NOT NULL,
                pp_base_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                pp_base_expired TIMESTAMP,
                CONSTRAINT PROD_PRICE_PK PRIMARY KEY (pp_id, pp_product_id)
);
COMMENT ON TABLE product_price IS 'Tabla con la informacion de las actualizaciones de precio para los productos';


CREATE SEQUENCE SUP_EMPLOYEES_SE_EMP_ID_SEQ;

CREATE TABLE sup_employees (
                se_emp_id NUMBER NOT NULL,
                se_sup_id NUMBER NOT NULL,
                se_pos_id NUMBER NOT NULL,
                se_fname VARCHAR2(20) NOT NULL,
                se_flastname VARCHAR2(40) NOT NULL,
                se_slastname VARCHAR2(40),
                se_gender_id NUMBER NOT NULL,
                se_email VARCHAR2(90) NOT NULL,
                se_phone_1_id NUMBER NOT NULL,
                se_phone_2_id NUMBER,
                CONSTRAINT SUP_EMP_PK PRIMARY KEY (se_emp_id, se_sup_id, se_pos_id)
);
COMMENT ON TABLE sup_employees IS 'Tabla con los empleados contactables de los proveedores disponibles para los almacenes';

create sequence branch_office_branch_id_seq;

CREATE TABLE branch_office (
                branch_id NUMBER NOT NULL,
                branch_add_id NUMBER NOT NULL,
                branch_open_date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                branch_name VARCHAR2(30) NOT NULL,
                branch_active CHAR(1) DEFAULT 'Y' NOT NULL,
                CONSTRAINT B_OFFICE_PK PRIMARY KEY (branch_id)
);
COMMENT ON TABLE branch_office IS 'Tabla con la informacion de los locales disponibles de la empresa';


CREATE TABLE branch_inventory (
		bi_id number not null,
                bi_branch_id NUMBER NOT NULL,
                bi_product_id NUMBER NOT NULL,
                bi_quantity NUMBER NOT NULL,
                bi_critical_q NUMBER NOT NULL,
		bi_product_code varchar2(17) not null,
		bi_product_expire TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                CONSTRAINT BRANCH_INV_PK PRIMARY KEY (bi_id, bi_branch_id, bi_product_id, bi_product_code)
);

create sequence branch_inventory_id_seq;


CREATE SEQUENCE SUPPLIER_ORDERS_SO_ID_SEQ;

CREATE TABLE supplier_orders (
                so_id NUMBER NOT NULL,
                so_status_id NUMBER NOT NULL,
                so_branch_id NUMBER NOT NULL,
                so_sa_id NUMBER NOT NULL,
                so_placed TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                so_received_date TIMESTAMP WITH TIME ZONE,
                so_received_code NUMBER NOT NULL,
                CONSTRAINT SUP_ORD_PK PRIMARY KEY (so_id)
);
COMMENT ON TABLE supplier_orders IS 'Tabla con la informacion de las ordenes de compra realizadas a los proveedores';


CREATE SEQUENCE SOD_PRODUCT_ID_SEQ;

CREATE TABLE supplier_order_detail (
                sod_id NUMBER NOT NULL,
                sod_quantity NUMBER NOT NULL,
                sod_prod_un_id NUMBER NOT NULL,
                sod_product_id NUMBER NOT NULL,
                sod_price_each NUMBER NOT NULL,
                CONSTRAINT SUPP_ORD_DET_PK PRIMARY KEY (sod_id)
);
COMMENT ON TABLE supplier_order_detail IS 'Tabla con la informacion del detalle de las ordenes de compra';


CREATE SEQUENCE EMPLOYEE_EMP_ID_SEQ;

CREATE TABLE employee (
                emp_id NUMBER NOT NULL,
                emp_fname VARCHAR2(30) NOT NULL,
                emp_lname VARCHAR2(60) NOT NULL,
                emp_slname VARCHAR2(60),
                emp_rut NUMBER NOT NULL,
                emp_rut_dv CHAR(1) NOT NULL,
                emp_gender_id NUMBER NOT NULL,
                emp_email VARCHAR2(90) NOT NULL,
                emp_phone_id NUMBER NOT NULL,
                emp_birthday TIMESTAMP WITH TIME ZONE NOT NULL,
                emp_created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                emp_hire_date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                emp_active CHAR(1) DEFAULT 'Y' NOT NULL,
                emp_role_id NUMBER NOT NULL,
                emp_add_id NUMBER NOT NULL,
                emp_supervisor_id NUMBER DEFAULT null,
                emp_branch_id NUMBER NOT NULL,
                CONSTRAINT EMP_PK PRIMARY KEY (emp_id)
);
COMMENT ON TABLE employee IS 'Tabla con la informacion de los empleados de los locales';


CREATE UNIQUE INDEX employee_rut_idk
 ON employee
 ( emp_rut ASC );

create sequence product_audit_prod_au_id_seq;

CREATE TABLE product_audit (
                prod_au_id NUMBER NOT NULL,
                prod_au_emp_id NUMBER NOT NULL,
                prod_au_date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                prod_au_orginal_values CLOB NOT NULL,
                prod_au_new_values CLOB NOT NULL,
                CONSTRAINT PROD_AUDIT_PK PRIMARY KEY (prod_au_id, prod_au_emp_id)
);
COMMENT ON TABLE product_audit IS 'Tabla para monitorear los cambios realizados en los productos';


CREATE SEQUENCE EMPLOYEE_AUDIT_EAUDIT_ID_SEQ;

CREATE TABLE employee_audit (
                eaudit_id NUMBER NOT NULL,
                eaudit_emp_id NUMBER NOT NULL,
                eaudit_date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                eaudit_original_values CLOB NOT NULL,
                eaudit_new_values CLOB NOT NULL,
                CONSTRAINT EMP_AUDIT_PK PRIMARY KEY (eaudit_id, eaudit_emp_id)
);
COMMENT ON TABLE employee_audit IS 'Tabla con la informacion de los cambios realizados a la tabla de empleados';

create sequence so_sup_order_id_seq;

CREATE TABLE supplier_audit (
                sa_id NUMBER NOT NULL,
                sa_emp_id NUMBER NOT NULL,
                sa_date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                sa_original_values CLOB NOT NULL,
                sa_new_values CLOB NOT NULL,
                CONSTRAINT SUPP_AUD_PK PRIMARY KEY (sa_id, sa_emp_id)
);
COMMENT ON TABLE supplier_audit IS 'Tabla con la informacion de los cambios realizados a la tabla de proveedores';


CREATE SEQUENCE CUSTOMERS_USER_ID_SEQ;

CREATE TABLE customers (
                cus_id NUMBER NOT NULL,
                cus_fname VARCHAR2(20) DEFAULT 'NO_NAME' NOT NULL,
                cus_flastname VARCHAR2(40) DEFAULT 'NO_LAST_NAME' NOT NULL,
                cus_slastname VARCHAR2(40) DEFAULT 'NO_SECOND_LAST_NAME',
                cus_rut NUMBER NOT NULL,
                cus_rut_dv VARCHAR2(1) NOT NULL,
                cus_gender_id NUMBER NOT NULL,
                cus_birthday TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                cus_email VARCHAR2(90) NOT NULL,
                cus_add_id NUMBER NOT NULL,
                cus_is_active CHAR(1) DEFAULT 'N' NOT NULL,
                cus_created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                cus_has_credit CHAR(1) DEFAULT 'N' NOT NULL,
                cus_credit_limit NUMBER,
                cus_credit_pay_day NUMBER DEFAULT 4 NOT NULL,
                cus_phone_id NUMBER,
                CONSTRAINT CUST_PK PRIMARY KEY (cus_id)
);
COMMENT ON TABLE customers IS 'Tabla con la informacion de los clientes';


CREATE UNIQUE INDEX customers_rut_idx
 ON customers
 ( cus_rut ASC );

CREATE SEQUENCE CUSTOMER_AUDIT_CAUD_ID_SEQ;

CREATE TABLE customer_audit (
                caud_id NUMBER NOT NULL,
                caud_cus_id NUMBER NOT NULL,
                caud_emp_id NUMBER NOT NULL,
                caud_date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                caud_original_values CLOB NOT NULL,
                caud_new_values CLOB NOT NULL,
                CONSTRAINT CAUD_PK PRIMARY KEY (caud_id, caud_cus_id, caud_emp_id)
);
COMMENT ON TABLE customer_audit IS 'Tabla con la informacion de los cambios realizados a la tabla clientes';


CREATE SEQUENCE CUST_ORDERS_ORDER_ID_SEQ;

CREATE TABLE cust_orders (
                order_id NUMBER NOT NULL,
                order_date_placed TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                order_date_paid TIMESTAMP WITH TIME ZONE,
                order_total_price NUMBER NOT NULL,
                order_customer_id NUMBER NOT NULL,
                order_payment_id NUMBER NOT NULL,
                order_branch_id NUMBER NOT NULL,
                CONSTRAINT CUST_ORD_PK PRIMARY KEY (order_id)
);
COMMENT ON TABLE cust_orders IS 'Tabla con la informacion de los las compras realizadas por los clientes';


CREATE SEQUENCE tickets_TICKET_ID_SEQ;

CREATE TABLE tickets (
                ticket_id NUMBER NOT NULL,
                ticket_order_id NUMBER NOT NULL,
                ticket_emited CHAR(1) DEFAULT 'Y' NOT NULL,
                ticket_emit_date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                ticket_code VARCHAR2(255),
                ticket_total_amount NUMBER DEFAULT 0 NOT NULL,
                ticket_net_value NUMBER(6,2) NOT NULL,
                ticket_tax_value NUMBER(6,2) NOT NULL,
                CONSTRAINT TICKETS_PK PRIMARY KEY (ticket_id, ticket_order_id)
);
COMMENT ON TABLE tickets IS 'Tabla con la informacion de las boletas emitidas';

create sequence order_audit_id_seq;

CREATE TABLE order_audit (
                orau_order_id NUMBER NOT NULL,
                orau_os_id NUMBER NOT NULL,
                orau_emp_id NUMBER NOT NULL,
                orau_date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
                orau_original_values CLOB NOT NULL,
                orau_new_values CLOB NOT NULL,
                CONSTRAINT ORD_AUD_PK PRIMARY KEY (orau_order_id, orau_os_id, orau_emp_id)
);
COMMENT ON TABLE order_audit IS 'Tabla con la informacion de los cambios realizados sobre la tabla de compras';

create sequence cust_order_det_id_seq;

CREATE TABLE cust_order_det (
                cod_product_id NUMBER NOT NULL,
                cod_order_id NUMBER NOT NULL,
                cod_quantity_ordered NUMBER NOT NULL,
                cod_price_each NUMBER NOT NULL,
                CONSTRAINT CUST_ORD_DET_PK PRIMARY KEY (cod_product_id, cod_order_id)
);
COMMENT ON TABLE cust_order_det IS 'Tabla con la informacion de los detalles de la compra';


ALTER TABLE customers ADD CONSTRAINT GENDER_CUSTOMERS_FK
FOREIGN KEY (cus_gender_id)
REFERENCES gender (gender_id)
NOT DEFERRABLE;

ALTER TABLE employee ADD CONSTRAINT GENDER_EMPLOYEE_FK
FOREIGN KEY (emp_gender_id)
REFERENCES gender (gender_id)
NOT DEFERRABLE;

ALTER TABLE sup_employees ADD CONSTRAINT GENDER_SUP_EMPLOYEES_FK
FOREIGN KEY (se_gender_id)
REFERENCES gender (gender_id)
NOT DEFERRABLE;

ALTER TABLE permission ADD CONSTRAINT PERMISSION_GROUP_PERMISSION_FK
FOREIGN KEY (perm_pgroup_id)
REFERENCES perm_group (pgroup_id)
NOT DEFERRABLE;

ALTER TABLE role_perm ADD CONSTRAINT PERMISSION_ROLE_PERM_FK
FOREIGN KEY (perm_id)
REFERENCES permission (perm_id)
NOT DEFERRABLE;

ALTER TABLE products ADD CONSTRAINT PRODUCT_UNIT_PROD_FK
FOREIGN KEY (prod_un_id)
REFERENCES prod_unit (prod_un_id)
NOT DEFERRABLE;

ALTER TABLE supplier_order_detail ADD CONSTRAINT PRODUCT_UNIT_S_ORDER_DETAIL_FK
FOREIGN KEY (sod_prod_un_id)
REFERENCES prod_unit (prod_un_id)
NOT DEFERRABLE;

ALTER TABLE employee ADD CONSTRAINT PHONE_EMPLOYEE_FK
FOREIGN KEY (emp_phone_id)
REFERENCES phone (phone_id)
NOT DEFERRABLE;

ALTER TABLE sup_employees ADD CONSTRAINT PHONE_1_SUP_EMP_FK
FOREIGN KEY (se_phone_1_id)
REFERENCES phone (phone_id)
NOT DEFERRABLE;

ALTER TABLE sup_employees ADD CONSTRAINT PHONE_2_SUP_EMP_FK
FOREIGN KEY (se_phone_2_id)
REFERENCES phone (phone_id)
NOT DEFERRABLE;

ALTER TABLE suppliers ADD CONSTRAINT PHONE_SUPPLIERS_FK
FOREIGN KEY (sup_phone_id)
REFERENCES phone (phone_id)
NOT DEFERRABLE;

ALTER TABLE customers ADD CONSTRAINT PHONE_CUSTOMERS_FK
FOREIGN KEY (cus_phone_id)
REFERENCES phone (phone_id)
NOT DEFERRABLE;

ALTER TABLE supplier_orders ADD CONSTRAINT SUPPLIER_ORDER_STATUS_FK
FOREIGN KEY (so_status_id)
REFERENCES supplier_order_status (so_status_id)
NOT DEFERRABLE;

ALTER TABLE employee ADD CONSTRAINT ROLES_EMPLOYEE_FK
FOREIGN KEY (emp_role_id)
REFERENCES roles (role_id)
DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE role_perm ADD CONSTRAINT ROLES_ROLE_PERM_FK
FOREIGN KEY (role_id)
REFERENCES roles (role_id)
NOT DEFERRABLE;

ALTER TABLE cust_orders ADD CONSTRAINT CUST_PAYMENTS_ORDERS_FK
FOREIGN KEY (order_payment_id)
REFERENCES cust_payments (payment_id)
NOT DEFERRABLE;

ALTER TABLE products ADD CONSTRAINT BRANDS_PRODUCTS_FK
FOREIGN KEY (brand_id)
REFERENCES brands (brand_id)
NOT DEFERRABLE;

ALTER TABLE sup_employees ADD CONSTRAINT POSITION_SUP_EMPLOYEES_FK
FOREIGN KEY (se_pos_id)
REFERENCES position (pos_id)
NOT DEFERRABLE;

ALTER TABLE products ADD CONSTRAINT PRODUCT_TYPE_PRODUCTS_FK
FOREIGN KEY (prod_cat_id)
REFERENCES prod_category (prod_cat_id)
NOT DEFERRABLE;

ALTER TABLE prod_category ADD CONSTRAINT PRODUCT_CATEGORY_PARENT_FK
FOREIGN KEY (prod_cat_parent_id)
REFERENCES prod_category (prod_cat_id)
NOT DEFERRABLE;

ALTER TABLE order_audit ADD CONSTRAINT ORDER_STATUS_AUDIT_FK
FOREIGN KEY (orau_os_id)
REFERENCES order_status (os_id)
NOT DEFERRABLE;

ALTER TABLE city ADD CONSTRAINT REGION_CIUDAD_FK
FOREIGN KEY (city_district_id)
REFERENCES district (district_id)
NOT DEFERRABLE;

ALTER TABLE commune ADD CONSTRAINT CIUDAD_COMUNA_FK
FOREIGN KEY (com_city_id)
REFERENCES city (city_id)
NOT DEFERRABLE;

ALTER TABLE addresses ADD CONSTRAINT COMUNA_ADDRESSES_FK
FOREIGN KEY (add_com_id)
REFERENCES commune (com_id)
NOT DEFERRABLE;

ALTER TABLE customers ADD CONSTRAINT ADDRESSES_USERS_FK
FOREIGN KEY (cus_add_id)
REFERENCES addresses (add_id)
NOT DEFERRABLE;

ALTER TABLE employee ADD CONSTRAINT ADDRESSES_EMPLOYEE_FK
FOREIGN KEY (emp_add_id)
REFERENCES addresses (add_id)
NOT DEFERRABLE;

ALTER TABLE branch_office ADD CONSTRAINT ADDR_BRANCH_OFFICE_FK
FOREIGN KEY (branch_add_id)
REFERENCES addresses (add_id)
NOT DEFERRABLE;

ALTER TABLE suppliers ADD CONSTRAINT ADDRESSES_SUPPLIERS_FK
FOREIGN KEY (sup_add_id)
REFERENCES addresses (add_id)
NOT DEFERRABLE;

ALTER TABLE sup_employees ADD CONSTRAINT SUPPLIERS_SUP_EMPLOYEES_FK
FOREIGN KEY (se_sup_id)
REFERENCES suppliers (sup_id)
NOT DEFERRABLE;

ALTER TABLE products ADD CONSTRAINT SUPPLIERS_PRODUCTS_FK
FOREIGN KEY (sup_id)
REFERENCES suppliers (sup_id)
NOT DEFERRABLE;

ALTER TABLE supplier_orders ADD CONSTRAINT SUPPLIERS_SUPPLIER_ORDERS_FK
FOREIGN KEY (so_sa_id)
REFERENCES suppliers (sup_id)
NOT DEFERRABLE;

ALTER TABLE cust_order_det ADD CONSTRAINT PRODUCTS_ORDER_DETAILS_FK
FOREIGN KEY (cod_product_id)
REFERENCES products (product_id)
NOT DEFERRABLE;

ALTER TABLE supplier_order_detail ADD CONSTRAINT PRODUCTS_SOD_FK
FOREIGN KEY (sod_product_id)
REFERENCES products (product_id)
NOT DEFERRABLE;

ALTER TABLE product_price ADD CONSTRAINT PRODUCTS_PRODUCT_PRICE_FK
FOREIGN KEY (pp_product_id)
REFERENCES products (product_id)
NOT DEFERRABLE;

ALTER TABLE product_audit ADD CONSTRAINT PRODUCTS_PRODUCT_AUDIT_FK
FOREIGN KEY (prod_au_id)
REFERENCES products (product_id)
NOT DEFERRABLE;

ALTER TABLE branch_inventory ADD CONSTRAINT PRODUCTS_BRANCH_INVENTORY_FK
FOREIGN KEY (bi_product_id)
REFERENCES products (product_id)
NOT DEFERRABLE;

ALTER TABLE cust_orders ADD CONSTRAINT B_OFFICE_CUSTOMERS_ORDERS_FK
FOREIGN KEY (order_branch_id)
REFERENCES branch_office (branch_id)
NOT DEFERRABLE;

ALTER TABLE supplier_orders ADD CONSTRAINT B_OFFICE_SUPPLIER_ORDERS_FK
FOREIGN KEY (so_branch_id)
REFERENCES branch_office (branch_id)
NOT DEFERRABLE;

ALTER TABLE employee ADD CONSTRAINT B_OFFICE_EMPLOYEE_FK
FOREIGN KEY (emp_branch_id)
REFERENCES branch_office (branch_id)
NOT DEFERRABLE;

ALTER TABLE branch_inventory ADD CONSTRAINT BRANCH_OFFICE_BRANCH_INV_FK
FOREIGN KEY (bi_branch_id)
REFERENCES branch_office (branch_id)
NOT DEFERRABLE;

ALTER TABLE supplier_order_detail ADD CONSTRAINT SUPPLIER_ORDERS_DETAIL_FK
FOREIGN KEY (sod_id)
REFERENCES supplier_orders (so_id)
NOT DEFERRABLE;

ALTER TABLE supplier_audit ADD CONSTRAINT SUPPLIER_ORDERS_AUDIT_FK
FOREIGN KEY (sa_id)
REFERENCES supplier_orders (so_id)
NOT DEFERRABLE;

ALTER TABLE order_audit ADD CONSTRAINT EMPLOYEE_ORDER_AUDIT_FK
FOREIGN KEY (orau_emp_id)
REFERENCES employee (emp_id)
NOT DEFERRABLE;

ALTER TABLE supplier_audit ADD CONSTRAINT EMPLOYEE_SUPPLIER_AUDIT_FK
FOREIGN KEY (sa_emp_id)
REFERENCES employee (emp_id)
NOT DEFERRABLE;

ALTER TABLE customer_audit ADD CONSTRAINT EMPLOYEE_CUSTOMER_AUDIT_FK
FOREIGN KEY (caud_emp_id)
REFERENCES employee (emp_id)
NOT DEFERRABLE;

ALTER TABLE employee_audit ADD CONSTRAINT EMPLOYEE_EMPLOYEE_AUDIT_FK
FOREIGN KEY (eaudit_emp_id)
REFERENCES employee (emp_id)
NOT DEFERRABLE;

ALTER TABLE employee ADD CONSTRAINT EMPLOYEE_SUPERVISOR_FK
FOREIGN KEY (emp_supervisor_id)
REFERENCES employee (emp_id)
DEFERRABLE INITIALLY IMMEDIATE;

ALTER TABLE product_audit ADD CONSTRAINT EMPLOYEE_PRODUCT_AUDIT_FK
FOREIGN KEY (prod_au_emp_id)
REFERENCES employee (emp_id)
NOT DEFERRABLE;

ALTER TABLE cust_orders ADD CONSTRAINT USERS_CUSTOMERS_ORDERS_FK
FOREIGN KEY (order_customer_id)
REFERENCES customers (cus_id)
NOT DEFERRABLE;

ALTER TABLE customer_audit ADD CONSTRAINT CUSTOMERS_CUSTOMER_AUDIT_FK
FOREIGN KEY (caud_cus_id)
REFERENCES customers (cus_id)
NOT DEFERRABLE;

ALTER TABLE cust_order_det ADD CONSTRAINT CUST_ORDERS_DETAILS_FK
FOREIGN KEY (cod_order_id)
REFERENCES cust_orders (order_id)
NOT DEFERRABLE;

ALTER TABLE order_audit ADD CONSTRAINT CUST_ORDERS_AUDIT_FK
FOREIGN KEY (orau_order_id)
REFERENCES cust_orders (order_id)
NOT DEFERRABLE;

ALTER TABLE tickets ADD CONSTRAINT CUST_ORDERS_TICKERS_FK
FOREIGN KEY (ticket_order_id)
REFERENCES cust_orders (order_id)
NOT DEFERRABLE;

