insert into roles (role_name, role_description) values ('Administrador', 'Usuario con privilegios de administrador del sistema');
insert into roles (role_name, role_description) values ('Gerente', 'Usuario gerente de la empresa con acceso privilegiado al backoffice y sistema de ventas del sistema solo por debajo del administrador');
insert into roles (role_name, role_description) values ('Supervisor', 'Usuario supervisor de los empleados respecto al uso del backoffice y sistema de ventas');
insert into roles (role_name, role_description) values ('Empleado', 'Usuario con limitado uso del backoffice y sistema de ventas');
insert into roles (role_name, role_description) values ('Vendedor', 'Usuario con privilegios sólo para el sistema de ventas, clientes y algunos aspectos del inventario');
