# Oracle XE inside Docker

## Preparativos configuración local

1. Obviamente la máquina donde se vaya a llevar a cabo este proceso debe tener instalado docker.

2. Descargar el instalador oficial desde el [siguiente link](https://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/xe-prior-releases-5172097.html) ya que por el peso es muy grande para dejar en el repositorio

3. La estructura final debería ser parecida a la que se muestra a continuación:

```shell
{ORACLE_DOCKER_DIR}
|_ buildDockerImage.sh
|_ 11.2.0.2
  |_ Checksum.xe
  |_ Dockerfile.xe
  |_ checkDBStatus.sh
  |_ runOracle.sh
  |_ setPassword.sh
  |_ xe.rsp
  |_oracle-xe-11.2.0-1.0.x86_64.rpm.zip
```
4. Ejecuta el archivo que construye la imagen y que se encuentra almacenado en {ORACLE_DOCKER_DIR}. De acuerdo a tu instalación de docker es posible que requieras permisos de superusuario:

```shell
./{ORACLE_DOCKER_DIR}/buildDockerImage.sh -v 11.2.0.2 -x
```
5. Después de un par de minutos, la imagen es creada e instalada localmente. Puedes comprobarlo usando el comando: docker images

A partir de este punto ya puedes crear cuantos contenedores necesites de la imagen que se acaba de construir. Para ello, podemos ejecutar el siguiente comando. Más información acerca de los params usados aquí:

```shell
docker run -d --name oraclexe \
--shm-size=1g \
-p 1521:1521 -p 8080:8080 \
-e ORACLE_PWD=password \
-v /u01/app/oracle/oradata \
oracle/database:11.2.0.2-xe
```
Una vez ejecutado, la creación del contenedor configurará la base de datos de acuerdo a los parámetros establecidos. El proceso puede tomar alrededor de 3-5 minutos, como la ejecución se realizó usando la opción -d (Detached) se debe monitorear los logs del contenedor para saber cuando se puede comenzar a usar: 

```shell
docker container logs -tf oraclexe
```

Cuando veas el siguiente mensaje en los logs, quiere decir que la base de datos está lista:

```shell
#########################
DATABASE IS READY TO USE!
#########################
```
Ahora, si quieres activar el usuario/esquema HR puedes hacerlo conectándote a sqlplus del contenedor y ejecutando el comando que activa dicha cuenta:

```shell
docker exec -ti oraclexe sqlplus sys/password@//localhost:1521/XE as sysdba
ALTER USER hr IDENTIFIED BY password ACCOUNT UNLOCK;
```

### Exportar la imagen para disponibilizarla en la nube

Es necesario salvar la imagen de Docker como un archivo tar:

```shell
docker save -o <path for generated tar file> <image name>
```

Luego copiar la imagen generada al nuevo sistema con el protocolo de transferencia que se desee. Luego debería ser posible cargar la imagen en docker utilizando:

```shell
docker load -i <path to image tar file>
PS: Puede ser necesario usar comandos con sudo.
```
Deberías agregar el nombre del archivo (no sólo el directoriio) con el comando -o, por ejemplo:

```shell
docker save -o c:/myfile.tar centos:16
```